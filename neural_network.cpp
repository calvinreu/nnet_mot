#include "neural_network.hpp"

nnet::MoT::neural_network::neural_network(const size_t &input_neurons, const size_t &hidden_neurons, const size_t &output_neurons, double (*akt)(const double&))
{
    m_output.assign<size_t, double(*)(const double&)>(output_neurons, hidden_neurons, akt);
    m_hidden.assign<size_t, double(*)(const double&)>(hidden_neurons, output_neurons, akt);
    m_input.assign<double(*)(const double&)>(input_neurons, akt);
    
    for (auto i = m_output.begin(); i < m_output.end(); i++)
        for (size_t i1 = 0; i1 < hidden_neurons; i++)
            i->m_connections[i1].m_input = &m_hidden.begin()->operator[](i1).m_output;

    for (auto i = m_hidden.begin(); i < m_hidden.end(); i++)
        for (size_t i1 = 0; i1 < hidden_neurons; i++)
            i->begin()->m_connections[i1].m_input = &m_input[i1].m_output;

}

nnet::MoT::neural_network::neural_network(const nnet::MoT::neural_network_info &constructionInfo)
{

    m_output.assign(constructionInfo.outputLayer.size());
    m_hidden.assign(constructionInfo.hiddenLayer.size());
    m_input .assign(constructionInfo.inputLayer .size());

    auto i01 = constructionInfo.outputLayer.begin();
    vector<nnet::MoT::connection_info>::const_iterator i11;

    for (auto i = m_output.begin(); i < m_output.end(); i++)
    {
        i->m_connections.assign(i01->connections.size());
        i->m_akt = i01->aktivationFunction;

        i11 = i01->connections.begin();
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
        {
            switch (i11->startLayerType)
            {
            case hidden:
                i1->m_input = &(m_hidden[i11->startLayerValue][i11->startNeuron].m_output);
                break;
            case input:
                i1->m_input = &(m_input[i11->startNeuron].m_output);
                break;
            }

            i11++;
        }

        i01++;
    }

    auto ilayer0 = constructionInfo.hiddenLayer.begin();
    for (auto ilayer = m_hidden.begin(); ilayer < m_hidden.end(); ilayer++)
    {
        i01 = ilayer0->begin();

        for (auto i = ilayer->begin(); i < ilayer->end(); i++)
        {
            i->m_connections.assign(i01->connections.size());
            i->m_akt = i01->aktivationFunction;

            i11 = i01->connections.begin();
            for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            {
                switch (i11->startLayerType)
                {
                case hidden:
                    i1->m_input = &(m_hidden[i11->startLayerValue][i11->startNeuron].m_output);
                    break;
                case input:
                    i1->m_input = &(m_input[i11->startNeuron].m_output);
                    break;
                }

                i11++;
            }

            i01++;
        }

        ilayer0++;
    }

    for (size_t i = 0; i < m_input.size(); i++)
        m_input[i].m_akt = constructionInfo.inputLayer[i].aktivationFunction;
}

nnet::MoT::neural_network_info nnet::MoT::neural_network::get_info()
{
    nnet::MoT::neural_network_info retVal;

    retVal.outputLayer.assign(m_output.size());
    retVal.hiddenLayer.assign(m_hidden.size());
    retVal.inputLayer .assign(m_input .size());

    vector<nnet::MoT::connection_info>::normal_iterator i11;
    auto i01 = retVal.outputLayer.begin();
        
    for (auto i = m_output.begin(); i < m_output.end(); i++)
    {
        i01++;
        i11 = i01->connections.begin();

        i01->aktivationFunction = i->m_akt;
        i01->connections.assign(i->m_connections.size());

        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
        {
            i11++;

            if ((void*)i1->m_input >= (void*)&m_input.begin() && (void*)i1->m_input < (void*)&m_input.end()){
                i11->startLayerType = input;
                i11->startNeuron = get_positionIndex((byte*)(m_input.get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::input));//calculate neuron position with pointers
            }else{
                i11->startLayerType = hidden;

                for (auto i2 = m_hidden.rbegin(); i2 > m_hidden.rend(); i2--)//faster because usually the connections are connected to the layer before
                    if((void*)i1->m_input >= (void*)&i2->begin() && (void*)i1->m_input < (void*)&i2->end())
                    {
                        i11->startNeuron = get_positionIndex((byte*)(i2->get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::hidden));//calculate neuron position with pointers
                        i11->startLayerValue = i2 - m_hidden.begin();
                    }
            }
            
            i11->weight = i1->m_weight;
        }
    }

    auto iLayer0 = retVal.hiddenLayer.begin();
    for (auto iLayer = m_hidden.begin(); iLayer < m_hidden.end(); iLayer++)
    {
        iLayer0->assign(iLayer->size());
        i01 = iLayer0->begin();

        for (auto i = iLayer->begin(); i < iLayer->end(); i++)
        {
            i11 = i01->connections.begin();

            i01->aktivationFunction = i->m_akt;
            i01->connections.assign(i->m_connections.size());

            for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            {
                if ((void*)i1->m_input >= (void*)&m_input.begin() && (void*)i1->m_input < (void*)&m_input.end()){
                    i11->startLayerType = input;
                    i11->startNeuron = get_positionIndex((byte*)(m_input.get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::input));//calculate neuron position with pointers
                }else{
                    i11->startLayerType = hidden;

                    iLayer--;//check previous layer first often faster

                    if((void*)i1->m_input >= (void*)&iLayer->begin() && (void*)i1->m_input < (void*)&iLayer->end())
                    {
                        i11->startNeuron = get_positionIndex((byte*)(iLayer->get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::hidden));//calculate neuron position with pointers
                        i11->startLayerValue = iLayer - m_hidden.begin();
                        iLayer++;
                    }
                    else
                    {
                        iLayer++;
                        for (auto i2 = m_hidden.begin(); i2 < m_hidden.end(); i2++)
                            if((void*)i1->m_input >= (void*)&i2->begin() && (void*)i1->m_input < (void*)&i2->end())
                            {
                                i11->startNeuron = get_positionIndex((byte*)(i2->get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::hidden));//calculate neuron position with pointers
                                i11->startLayerValue = i2 - m_hidden.begin();
                            }
                    }
                }

                i11->weight = i1->m_weight;

                i11++;
            }

            i01++;
        }

        iLayer0++;
    }

    for (size_t i = 0; i < m_input.size(); i++)
        retVal.inputLayer[i].aktivationFunction = m_input[i].m_akt;

    return retVal;
}

nnet::neural_network_info nnet::MoT::neural_network::get_standard_info()
{
    nnet::MoT::neural_network_info NNinfo = this->get_info();
    nnet::neural_network_info retVal;

    size_t temp = 0;
    vector<size_t> tempV;
    vector<nnet::hidden_neuron_info>::normal_iterator i01;
    vector<nnet::connection_info>::normal_iterator i21;

    retVal.inputLayer = std::move(NNinfo.inputLayer);

    tempV.assign(NNinfo.hiddenLayer.size());

    for (size_t i = 0; i < NNinfo.hiddenLayer.size(); i++){
        temp += NNinfo.hiddenLayer[i].size();
        tempV[i] = temp;
    }

    retVal.hiddenLayer.assign(temp);
    retVal.outputLayer.assign(NNinfo.outputLayer.size());

    i01 = retVal.outputLayer.begin();

    for (auto i = NNinfo.outputLayer.begin(); i < NNinfo.outputLayer.end(); i++)
    {
        i01->aktivationFunction = i->aktivationFunction;
        i01->connections.assign(i->connections.size());
        
        i21 = i01->connections.begin();
        for (auto i2 = i->connections.begin(); i2 < i->connections.end(); i2++)
        {
            i21->startLayer = i2->startLayerType;
            i21->startNeuron = tempV[i2->startLayerValue] + i2->startNeuron;
            i21->weight = i2->weight;
            i21++;
        }
        
        i01++;
    }

    i01 = retVal.hiddenLayer.begin();

    for (auto i = NNinfo.hiddenLayer.begin(); i < NNinfo.hiddenLayer.end(); i++)
    {
        for (auto i1 = i->begin(); i1 < i->end(); i1++)
        {
            i01->aktivationFunction = i1->aktivationFunction;
            i01->connections.assign(i1->connections.size());
            i21 = i01->connections.begin();
            for (auto i2 = i1->connections.begin(); i2 < i1->connections.end(); i2++)
            {
                i21->startLayer = i2->startLayerType;
                i21->weight = i2->weight;
                i21->startNeuron = tempV[i2->startLayerValue] + i2->startNeuron;
                i21++;
            }
            i01++;
        }
    }

    return retVal;
}

void nnet::MoT::neural_network::run(double* output_location, double* input_location)
{
    for (auto i = m_input.begin(); i < m_input.end(); i++)
        i->update(*input_location);

    for (auto iLayer = m_hidden.begin(); iLayer < m_hidden.end(); iLayer++)
        for (auto i = iLayer->begin(); i < iLayer->end(); i++)
            i->update();

    for (auto i = m_output.begin(); i < m_output.end(); i++)
        i->update(*output_location);
    
}

void nnet::MoT::neural_network::remap_hidden(void* prevVectorPos)
{
    for(auto i = m_output.begin(); i < m_output.end(); i++)
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            for (auto i2 = m_hidden.rbegin(); i2 > m_hidden.rend(); i++)
                if((void*)&(i2->begin()) <= i1->m_input && i1->m_input > (void*)&(i2->end())){
                    i1->remap(prevVectorPos, i2->get_ValPtr());
                    break;
                }

    for (auto iLayer = m_hidden.begin(); iLayer < m_hidden.end(); iLayer++)
        for(auto i = iLayer->begin(); i < iLayer->end(); i++)
            for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            {
                iLayer--;//check previous layer first often faster

                if((void*)i1->m_input >= (void*)&iLayer->begin() && (void*)i1->m_input < (void*)&iLayer->end())
                {
                    i1->remap(prevVectorPos, iLayer->get_ValPtr());
                    iLayer++;
                }
                else
                {
                    iLayer++;
                    for (auto i2 = m_hidden.begin(); i2 < m_hidden.end(); i2++)
                        if((void*)i1->m_input >= (void*)&i2->begin() && (void*)i1->m_input < (void*)&i2->end())
                        {
                            i1->remap(prevVectorPos, m_hidden.get_ValPtr());
                        }
                }
            }
                //if((void*)(&iLayer->begin()) <= i1->m_input && i1->m_input > (void*)&(iLayer->end()))
                //    i1->remap(prevVectorPos, m_hidden.get_ValPtr());
}

void nnet::MoT::neural_network::remap_input(void* prevVectorPos)
{
    for(auto i = m_output.begin(); i < m_output.end(); i++)
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            if((void*)&(m_input.begin()) <= i1->m_input && i1->m_input > (void*)&(m_input.end()))
                i1->remap(prevVectorPos, m_input.get_ValPtr());

    for (auto iLayer = m_hidden.begin(); iLayer < m_hidden.end(); iLayer++)
        for(auto i = iLayer->begin(); i < iLayer->end(); i++)
            for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
                if((void*)&(m_input.begin()) <= i1->m_input && i1->m_input > (void*)&(m_input.end()))
                    i1->remap(prevVectorPos, m_input.get_ValPtr());
}