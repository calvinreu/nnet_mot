#include <vector.hpp>
#include <nnet/info_objects.hpp>

namespace nnet::MoT{

    struct connection_info
    {
        nnet::layer startLayerType;
        size_t startLayerValue;
        size_t startNeuron;
        double weight;
    };

    struct hidden_neuron_info
    {
        vector<nnet::MoT::connection_info> connections;
        double (*aktivationFunction)(const double&);
    };

    typedef hidden_neuron_info output_neuron_info;

    struct neural_network_info
    {
        vector<input_neuron_info>  inputLayer ;
        vector<vector<hidden_neuron_info>> hiddenLayer;
        vector<output_neuron_info> outputLayer;
    };
    
}