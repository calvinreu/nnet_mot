#include <iostream>
#include <vector.hpp>
#include <nnet/neuron.hpp>
#include <nnet/functions.hpp>
#include "info_objects.hpp"

namespace nnet::MoT{

    struct neural_network
    {
        vector<NEURON::input > m_input ;
        vector<vector<NEURON::hidden>> m_hidden;
        vector<NEURON::output> m_output;

        neural_network(const size_t &input_neurons, const size_t &hidden_neurons, const size_t &output_neurons, double (*akt)(const double&));//create standart neural network
        neural_network(const nnet::MoT::neural_network_info &constructionInfo);
        neural_network(){}
        ~neural_network(){}

        void run(double* output_location, double* input_location);
        void remap_hidden(void* prevVectorPos);
        void remap_input(void* prevVectorPos);

        nnet::MoT::neural_network_info get_info();
        nnet::neural_network_info get_standard_info();
    };

}
