CC = g++

default:
	make MoT
	make clean

MoT: neural_network.cpp
	$(CC) neural_network.cpp -c -g -fpic -lnnet -o neural_network.o
	gcc -shared neural_network.o -o libnnet_Mot.so

clean: neural_network.o
	rm -f neural_network.o

clean-all:
	rm -f /usr/lib/libnnet_Mot.so
	rm -f -r /usr/include/c++/9/nnet/MoT

install: libnnet_Mot.so neural_network.hpp info_objects.hpp
	make clean-all
	mkdir /usr/include/c++/9/nnet/MoT
	mv libnnet_Mot.so /usr/lib
	chmod 0755 /usr/lib/libnnet_Mot.so
	cp neural_network.hpp /usr/include/c++/9/nnet/MoT
	chmod 0755 /usr/include/c++/9/nnet/MoT neural_network.hpp
	cp info_objects.hpp /usr/include/c++/9/nnet/MoT
	chmod 0755 /usr/include/c++/9/nnet/MoT info_objects.hpp